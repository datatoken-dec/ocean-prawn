# Ocean PRAWN #



### What is Ocean PRAWN? ###

**Ocean PRAWN** (**P**ostman **R**esource & **A**PI **W**orkspace **N**exus) is a group of public workspaces 
consisting of APIs, source & sample code, libraries, SDKs, documentation, and **Postman** components dedicated to **Ocean** protocol and its partners.